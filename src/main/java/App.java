import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import org.bson.Document;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.include;

public class App {

    public static void main(String[] args) {
        try {
            // Connect to MongoDB
            MongoClient mongo = new MongoClient("localhost", 27017);

            // Get database
            MongoDatabase db = mongo.getDatabase("test");

            // Get collections
            MongoCollection<Document> collection = db.getCollection("users");

            // Find "Tom" and display
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("name", "Tom");
            displayCollection(collection, searchQuery);

            // Find first
            Document doc = collection.find().first();
            System.out.println("===== First doc ======");   
            System.out.println(doc.toJson());

            // Find age > 30 and display (Get set of documents with a query)
            System.out.println("========= Age above 30 ==========");
            collection.find(gt("age", 30)).forEach((Block<Document>) item -> System.out.println(item.toJson()));

            // Insert
//            doc = new Document("name", "Bob")
//                    .append("age", 54)
//                    .append("languages", Arrays.asList("english", "polish"));
//            collection.insertOne(doc);

            // Update one, even there are few users with age=28: set age=29
            collection.updateOne(eq("age", 28), new Document("$set", new Document("age", 29)));

            // Update many: increment users with age above 30 with +10
            collection.updateMany(gt("age", 30), new Document("$inc", new Document("age", 10)));


            displayCollection(collection);


        } catch (Exception ex) {

        }

    }

    private static void displayCollection(MongoCollection<Document> collection, BasicDBObject search) {
        MongoCursor<Document> cursor;
        String header;
        if (search == null) {
            cursor = collection.find().projection(and(include("name", "age"), excludeId())).iterator();
            header = "====== Display whole collection ======";
        } else {
            cursor = collection.find(search).projection(excludeId()).iterator();
            header = "====== Display collection with " + search.toJson() + " ==========";
        }

        System.out.println(header);
        try {
            while (cursor.hasNext()) {
                System.out.println(cursor.next().toJson());
            }
        } finally {
            cursor.close();
        }
        System.out.println("=============================");
    }

    private static void displayCollection(MongoCollection collection) {
        displayCollection(collection, null);
    }

}
